import requests
import json


def get_grid(api_address: str, api_key: str):
    # payload = {'api_key': api_key}
    url = f"http://{api_address}?api_key={api_key}"
    # print(f"URL is {url}")
    r = requests.get(url)
    # print(r.text)
    try:
        return json.loads(r.text)
    except json.JSONDecodeError:
        print(f"JSONDecodeError - data: {r.text}")
        return []


def set_cell(api_address: str, api_key: str, x: int, y: int, red: int, green: int, blue: int):
    # payload = {'api_key': api_key, 'x': x, 'y': y, 'red': red, 'green': green, 'blue': blue}
    r = requests.put(f"http://{api_address}?api_key={api_key}&x={x}&y={y}&red={red}&green={green}&blue={blue}")
    try:
        return json.loads(r.text)
    except json.JSONDecodeError:
        return []


if __name__ == '__main__':
    url = 'localhost:5000/grid1/api/v1'
    api_key = "fbd8a2bc-8743-4842-abef-9db981dbf24c"
    print()
    print(set_cell(url, api_key, x=23, y=34, red=255, green=0, blue=0))
