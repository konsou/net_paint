import pygame
import time
from pygame.math import Vector2
from pygame.locals import *


def generate_color_picker_image(current_color) -> pygame.Surface:
    width = 256
    height = 100

    current_color = list(current_color)

    surf = pygame.Surface((width, height))

    for index_ in range(3):
        for x in range(width):
            color_ = current_color.copy()
            color_[index_] = x
            pygame.draw.line(surf, color_, (x, index_ * height // 3), (x, index_ * height // 3 + (height - 1)))

    return surf


if __name__ == '__main__':
    current_color = 0, 0, 0
    pygame.init()
    win = pygame.display.set_mode((512, 512))
    image = color_picker_image(current_color)
    # image_red = color_picker_image((127, 127, 127), 'red')
    # image_green = color_picker_image((127, 127, 127), 'green')
    # image_blue = color_picker_image((127, 127, 127), 'blue')
    # win.blit(image_red, (0, 0))
    # win.blit(image_green, (0, 20))

    running = True

    while running:
        for event in pygame.event.get():
            if event.type == QUIT:
                running = False
            elif event.type == KEYUP:
                if event.key == K_ESCAPE:
                    running = False
            elif event.type == MOUSEBUTTONUP:
                try:
                    current_color = image.get_at(pygame.mouse.get_pos())
                    print(current_color)
                    image = color_picker_image(current_color)
                except IndexError:
                    pass

        time.sleep(0.01)
        pygame.draw.rect(win, current_color, (300, 0, 100, 100))
        win.blit(image, (0, 0))
        pygame.display.flip()

