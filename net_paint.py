import flask
from flask import Flask, render_template, flash, request, send_from_directory
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField, IntegerField
import api_interface


app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config['SECRET_KEY'] = "sdfhjf3872oiwhefbkyufga238ogo23"


class InfoForm(Form):
    api_address = StringField('API address:', validators=[validators.InputRequired()])
    api_key = StringField('API key:', validators=[validators.InputRequired()])
    red = IntegerField('Red:')
    green = IntegerField('Green:')
    blue = IntegerField('Blue:')


@app.route('/js/<path:path>')
def js(path):
    return send_from_directory('js', path)


@app.route('/', methods=['GET', 'POST'])
def main_view():
    form = InfoForm(request.form)

    red, green, blue = None, None, None
    print(form.errors)

    grid = []

    if request.method == 'POST' and form.validate():

        api_address = request.form['api_address']
        api_key = request.form['api_key']
        red = request.form['red']
        green = request.form['green']
        blue = request.form['blue']

        if api_address is not None and api_key is not None:
            grid = api_interface.get_grid(api_address, api_key)

    if red is None:
        red = 0
    if green is None:
        green = 0
    if blue is None:
        blue = 0

    print(f"red: {red}, green: {green}, blue: {blue}")

    return render_template('frontpage.html', form=form, grid=grid,
                           red=red, green=green, blue=blue)


if __name__ == '__main__':
    app.run(port=5001)
