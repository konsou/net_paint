import pygame


class Text(pygame.sprite.Sprite):
    """ Utilityclass tekstin näyttämiseen ruudulla """
    def __init__(self, pos, text: str, group, color=(255, 255, 255), bgcolor=(0, 0, 0), font_size=24):
        pygame.sprite.Sprite.__init__(self, group)
        self._pos = pos
        self._color = color
        self._bgcolor = bgcolor
        self._font_size = font_size

        # This updates the image
        self.text = text

        # This updates rect position
        self.pos = pos

    def _get_pos(self):
        return self._pos

    def _set_pos(self, pos):
        self._pos = pos
        self.rect.center = pos

    def _get_text(self):
        return self._text

    def _set_text(self, new_text: str):
        self._text = new_text
        font_object = pygame.font.Font(None, self._font_size)
        self.image = font_object.render(self._text, 1, self._color, self._bgcolor)
        self.rect = self.image.get_rect()
        self.rect.center = self._pos

    pos = property(_get_pos, _set_pos)
    text = property(_get_text, _set_text)

