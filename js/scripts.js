$( document ).ready(function() {
    console.log( "ready!" );

    $(".grid-cell").click(function(){
        //console.log(this.id)
        const id_split = this.id.split("-");
        const x = id_split[1];
        const y = id_split[2];
        const red = $("#red").val();
        const green= $("#green").val();
        const blue = $("#blue").val();
        console.log("Clicked x: "+ x + " y: "+ y + " - RGB: " + red + " " + green + " " + blue);

        api_url = "http://" + $("#api_address").val();
        api_key = $("#api_key").val();
        query_string = api_url+"?x="+x+"&y="+y+"&red="+red+"&green="+green+"&blue="+blue+"&api_key="+api_key;
        console.log(query_string);
        $.ajax({
            url: query_string,
            type: 'PUT',

            success: function(result) {
                console.log(result);
            };
        });

    });

});

