import pygame
import json
from pygame.locals import *
import api_interface
from text import Text
from pygame_color_picker import generate_color_picker_image


def generate_image(api_url: str, api_key: str, scaling: int=1) -> pygame.Surface:
    print(f"Fetching grid data from {api_url}...")
    grid = api_interface.get_grid(api_address=api_url, api_key=api_key)
    print(f"Done!")
    image_surface = pygame.Surface((100, 100))

    for cell in grid:
        image_surface.set_at((cell['x'], cell['y']),
                             (cell['red'], cell['green'], cell['blue']))

    return pygame.transform.scale(image_surface, (100 * scaling, 100 * scaling))


class JustAnImage(pygame.sprite.Sprite):
    def __init__(self, image, group):
        pygame.sprite.Sprite.__init__(self, group)
        self.image = image
        self.rect = image.get_rect()


if __name__ == '__main__':
    with open("settings.json") as f:
        settings = json.load(f)
    print(settings)
    api_key = settings['api_key']
    api_url = settings['api_url']

    pygame.init()
    ui_group = pygame.sprite.Group()

    IMAGE_SCALING = 7

    win = pygame.display.set_mode((100 * IMAGE_SCALING + 300, 100 * IMAGE_SCALING))

    image = generate_image(api_url, api_key, scaling=IMAGE_SCALING)
    picked_color = 127, 127, 127
    color_picker = JustAnImage(generate_color_picker_image(picked_color), ui_group)
    color_picker.rect.topleft = IMAGE_SCALING * 100 + 20, 20

    clock = pygame.time.Clock()

    x_y_text = Text((100 * IMAGE_SCALING + 100, 200), "(x, y)", group=ui_group)

    print(len(ui_group))
    running = True

    picked_color = 0, 0, 0

    mouse_cell_x, mouse_cell_y = 0, 0
    last_drawn_x, last_drawn_y = None, None

    while running:
        mouse_pos = pygame.mouse.get_pos()

        mouse_cell_x_previous = mouse_cell_x
        mouse_cell_y_previous = mouse_cell_y
        mouse_cell_x = mouse_pos[0] // IMAGE_SCALING
        mouse_cell_y = mouse_pos[1] // IMAGE_SCALING

        for event in pygame.event.get():
            if event.type == QUIT:
                running = False
            elif event.type == KEYUP:
                if event.key == K_ESCAPE:
                    running = False
            elif event.type == MOUSEBUTTONUP:
                if not (0 <= mouse_cell_x <= 99 and 0 <= mouse_cell_y <= 99):
                    mouse_x, mouse_y = mouse_pos
                    try:
                        picked_color = win.get_at(pygame.mouse.get_pos())
                        print(picked_color)
                        color_picker.image = generate_color_picker_image(picked_color)
                    except IndexError:
                        pass

        if pygame.mouse.get_pressed()[0]:
            if 0 <= mouse_cell_x <= 99 and 0 <= mouse_cell_y <= 99:
                if (last_drawn_x is None and last_drawn_y is None) or \
                        mouse_cell_x != last_drawn_x or mouse_cell_y != last_drawn_y:
                    new_cell = api_interface.set_cell(api_address=api_url, api_key=api_key,
                                                      x=mouse_cell_x, y=mouse_cell_y,
                                                      red=picked_color[0], green=picked_color[1], blue=picked_color[2])
                    print(new_cell)
                    response_color = new_cell['red'], new_cell['green'], new_cell['blue']
                    cell_x = new_cell['x']
                    cell_y = new_cell['y']
                    pygame.draw.rect(image, response_color, (cell_x * IMAGE_SCALING, cell_y * IMAGE_SCALING,
                                                             IMAGE_SCALING, IMAGE_SCALING))
                    # image = generate_image(api_url, api_key, scaling=IMAGE_SCALING)
                    last_drawn_x = mouse_cell_x
                    last_drawn_y = mouse_cell_y


        x_y_text.text = f"x: {mouse_cell_x}, y: {mouse_cell_y}"

        win.fill((0, 0, 0))

        pygame.draw.rect(win, picked_color, (100 * IMAGE_SCALING + 20, 140, 256, 30))

        win.blit(image, (0, 0))
        # win.blit(color_picker_image, (100 * IMAGE_SCALING + 10, 20))
        ui_group.draw(win)

        pygame.display.flip()
        clock.tick(30)

